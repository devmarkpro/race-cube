export const format = (str: string, params: string[]): string => {
  // for(let i = 0 ; i < params.length; i++) {
  //     str = str.replace(new RegExp('{' + i + '}', 'g'), params[i]);
  // }
  // return str;

  for (let i = 0; i < params.length; i++) {
    const regEx = new RegExp('\\{' + i + '\\}', 'gm');
    str = str.replace(regEx, params[i]);
  }

  return str;
};
