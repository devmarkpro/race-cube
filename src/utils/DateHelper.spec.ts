import moment from 'moment';
import { findNearestDate } from './DateHelper';
describe('# => find nearest date', () => {
  test('## => should return nearest date index', () => {
    const d = '2019-10-10 00:00:00';
    const date = moment('2019-10-10 00:00:00').toDate();
    const dates = [
      moment(d)
        .add(-2, 'days')
        .toDate(),
      moment(d)
        .add(5, 'day')
        .toDate(),
      moment(d)
        .add(-1, 'day')
        .toDate(),
      moment(d)
        .add(20, 'minutes')
        .toDate(),
      moment(d)
        .add(1, 'day')
        .toDate(),
    ];
    const nearestIndex = findNearestDate(dates, date);
    expect(nearestIndex).toBe(3);
  });
});
describe('# => find nearest date', () => {
  it('## => should return -1 for all past days', () => {
    const d = '2019-10-10 00:00:00';
    const date = moment('2019-10-10 00:00:00').toDate();
    const dates = [
      moment(d)
        .add(-2, 'days')
        .toDate(),
      moment(d)
        .add(-5, 'day')
        .toDate(),
      moment(d)
        .add(-1, 'day')
        .toDate(),
      moment(d)
        .add(-20, 'minutes')
        .toDate(),
      moment(d)
        .add(-1, 'second')
        .toDate(),
    ];
    const nearestIndex = findNearestDate(dates, date);
    expect(nearestIndex).toBe(-1);
  });
});
