import winston from 'winston';
import { configs } from '../config';
import expressWinston from 'express-winston';
const transporters = [
  new winston.transports.File({
    filename: 'error.log',
    level: 'error',
  }),
  new winston.transports.File({
    filename: 'app.log',
  }),
];
const winstonLogger = winston.createLogger({
  level: 'silly',
  format: winston.format.combine(
    winston.format.timestamp(),
    winston.format.json(),
  ),

  transports: transporters,
});
const getMeta = (meta?: any | null): object => {
  let m = { SERVICE: 'CONTEST' };
  if (meta) {
    m = { ...m, ...meta };
  }
  return m;
};
winstonLogger.level = configs.log.LOG_LEVEL || 'debug';
winstonLogger.add(new winston.transports.Console());

export const silly = (msg: string, meta?: any, callback?: () => void) => {
  winstonLogger.silly(msg, getMeta(meta), () => {
    if (callback) {
      callback();
    }
  });
};
export const warn = (msg: string, meta?: any, callback?: () => void) => {
  winstonLogger.warn(msg, getMeta(meta), () => {
    if (callback) {
      callback();
    }
  });
};

export const error = (msg: string, meta?: any, callback?: () => void) => {
  winstonLogger.error(msg, getMeta(meta), () => {
    if (callback) {
      callback();
    }
  });
};

export const info = (msg: string, meta?: any, callback?: () => void) => {
  winstonLogger.info(msg, getMeta(meta), () => {
    if (callback) {
      callback();
    }
  });
};
export const verbose = (msg: string, meta?: any, callback?: () => void) => {
  winstonLogger.verbose(msg, getMeta(meta), () => {
    if (callback) {
      callback();
    }
  });
};
export const debug = (
  msg: any[] | string,
  meta?: any,
  callback?: () => void,
) => {
  let m = [];
  if (typeof msg === 'string') {
    m.push(msg);
  } else {
    m = msg;
  }
  winstonLogger.debug(m.join(' '), getMeta(meta), () => {
    if (callback) {
      callback();
    }
  });
};

export const middlewareConfig = () => {
  transporters.push(
    new winston.transports.File({
      filename: 'http.log',
      level: configs.log.LOG_LEVEL || 'debug',
    }),
  );
  return expressWinston.logger({
    transports: transporters,
    meta: true,
    // @ts-ignore
    format: winston.format.combine(
      winston.format.colorize(),
      winston.format.json(),
    ),
    msg:
      'HTTP {{res.statusCode}} {{req.method}} {{res.responseTime}}ms {{req.url}}',
    expressFormat: true,
    // ignoreRoute: (req, res) => {
    //   return true;
    // },
  });
};

export default winstonLogger;
