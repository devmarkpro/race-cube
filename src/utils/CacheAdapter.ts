import { getRedisInstance } from './RedisAdapter';
const redis = getRedisInstance();
type CacheScopes = 'CONTEST_META' | 'CONTEST';
export default class CacheAdapter {
  private appId: string;
  private PREFIXES = {
    CONTEST_META: 'contest:meta',
  };
  constructor(appId: string) {
    this.appId = appId;
  }
  private getKey = (scope: CacheScopes): string => {
    // @ts-ignore
    return this.PREFIXES[scope];
  };
}
