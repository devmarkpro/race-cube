import fs from 'fs';
import path from 'path';
const secret = fs.readFileSync(
  path.resolve('src', 'secrets', 'moneyket-public.pem'),
);
const expressJwt = require('express-jwt');
module.exports = jwt;

function jwt() {
  return expressJwt({ secret, isRevoked }).unless({
    path: ['/', '/v1/user/config'],
  });
}

async function isRevoked(req: any, payload: any, done: any) {
  done();
}
