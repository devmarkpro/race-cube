import { format } from './StringFormat';

export class AppErrors extends Error {
  public static OBJECT_NOT_FOUND = {
    MSG: 'Cannot found any {0} with "{1}"',
    STATUS: 404,
  };
  public static INCORRECT_CORRECT_ANSWER_INDEX = {
    MSG: 'Index of correct answers are wrong',
    STATUS: 400403,
  };
  public static CONTEST_IS_PAST = {
    MSG: 'You cannot define questions for this contest anymore',
    STATUS: 400,
  };
  public static APP_IS_REQUIRED = {
    MSG: 'App is required',
    STATUS: 400,
  };
  public static UNEXPECTED_ERROR = {
    MSG:
      'Something bad happened which we do not know nothing about that right now!',
    STATUS: 500,
  };
}
export const MakeError = (appError: any, ...params: string[]): AppErrors => {
  const err = new AppErrors();
  err.message = format(appError.MSG, params);
  err.name = appError.STATUS;
  return err;
};
