import BrokerInterface from '../BrokerInterface';
import axios from 'axios';
import { configs } from '../../../config';
import * as logger from '../../../utils/Logger';
import { MessageResponse } from '../../../../typings/app-response';
export interface PublishRequest {
  topic: string;
  payload?: MessageResponse;
  qos?: number;
  retain?: boolean;
  clientId?: string;
}
const urls = {
  PUBLISH: '/mqtt/publish',
};
const axiosInstance = axios.create({
  baseURL: configs.app.EMQ_BASE,
  timeout: 1000,
  auth: {
    username: configs.app.EMQ_APP_ID,
    password: configs.app.EMQ_APP_SECRET,
  },
});
export class EMQBroker implements BrokerInterface<PublishRequest> {
  public async Publish(request: PublishRequest): Promise<boolean> {
    try {
      const obj: {
        topic: string;
        payload?: string;
        client_id?: string;
        retain?: boolean;
        qos?: number;
      } = {
        topic: request.topic,
      };
      if (request.clientId) {
        obj.client_id = request.clientId;
      }
      if (request.payload) {
        obj.payload = JSON.stringify(request.payload);
      }
      if (request.retain) {
        obj.retain = request.retain;
      }
      if (request.qos) {
        obj.qos = request.qos;
      }
      await axiosInstance.post(urls.PUBLISH, obj);
      logger.debug(`Publish message successfully in topic: ${request.topic}`);
      return true;
    } catch (e) {
      logger.warn('Failed to publish message');
      logger.error(e);
      throw e;
    }
  }
  public async BatchPublish(requests: PublishRequest[]): Promise<number> {
    try {
      requests.forEach(req =>
        axiosInstance.post(urls.PUBLISH, {
          topic: req.topic,
          payload: req.payload,
          client_id: req.clientId,
        }),
      );
      return 1;
    } catch (e) {
      logger.warn('Failed to publish message');
      logger.error(e);
      return 0;
    }
  }
  public async GetAllSubs(): Promise<any> {
    return axiosInstance.get('subscriptions');
  }
}
