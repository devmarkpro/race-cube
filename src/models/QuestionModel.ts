import { ContestMetaModel } from './ContestMetaModel';

export interface QuestionModel {
  order: number;
  text: string;
  options: any[];
  meta: ContestMetaModel;
}
