import BrokerInstance from '../services/Broker';
import moment from 'moment-timezone';
import { ContestModel, UserCreditModel } from '../schemas';
export default async (job: any) => {
  const broker = BrokerInstance();
  const contest = await ContestModel.findById(job.data.contestId);
  if (!contest) {
    return Promise.resolve(job);
  }
  const time = moment(contest.beginTime).tz('Asia/Tehran');
  broker.Publish({
    topic: contest._id,
    payload: {
      type: 'DEAD_LINE',
      data: {
        prize: contest.prize,
        secondsToStart: time.diff(moment().tz('Asia/Tehran'), 's'),
      },
    },
  });
  return Promise.resolve(job);
};
