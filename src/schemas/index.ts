import mongoose, { Model, model, Document } from 'mongoose';
import { configs } from '../config';
import { debug, error } from '../utils/Logger';
import ContestMetaSchema from './ContestMetaSchema';
import UserTrackSchema from './UserTrack';
import UserCreditSchema from './UserCredit';
import * as AppModels from '../models';
import ModelNames from './ModelNames';

export default (): void => {
  debug('Connecting to Mongo ' + configs.app.MONGO_URI);
  mongoose
    .connect(
      configs.app.MONGO_URI,
      {
        useNewUrlParser: true,
        useFindAndModify: false,
      },
    )
    .then(() => {
      debug('Connected to Mongo');
    })
    .catch((err: any) => {
      debug('Failed to connect to Mongo');
      error(err);
    });
};
// const ContestMetaModel = mongoose.model(Names.CONTEST, ContestMetaSchema);
// const QuestionModel = mongoose.model(Names.QUESTION, QuestionSchema);
// const AnswerModel = mongoose.model(Names.ANSWER, AnswerSchema);
// const UserTrackModel = mongoose.model(Names.USER_TRACK, UserTrackSchema);

interface ContestAppModel extends AppModels.ContestMetaModel, Document {}
const ContestModel: Model<ContestAppModel> = model<ContestAppModel>(
  ModelNames.CONTEST,
  ContestMetaSchema,
);

interface UserTrackAppModel extends AppModels.UserTrackModel, Document {}
const UserTrackModel: Model<UserTrackAppModel> = model<UserTrackAppModel>(
  ModelNames.USER_TRACK,
  UserTrackSchema,
);

interface UserCreditAppModel extends AppModels.UserCreditModel, Document {}
const UserCreditModel: Model<UserCreditAppModel> = model<UserCreditAppModel>(
  ModelNames.USER_CREDIT,
  UserCreditSchema,
);

export { ContestModel, UserTrackModel, UserCreditModel };
