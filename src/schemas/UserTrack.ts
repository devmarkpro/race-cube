import { Schema } from 'mongoose';
import Names from './ModelNames';
import mongooseDelete = require('mongoose-delete');
const UserTrackSchema = new Schema(
  {
    userId: {
      type: String,
      required: true,
    },
    contestId: {
      type: Schema.Types.ObjectId,
      ref: Names.CONTEST,
      require: true,
    },
    questionIndex: {
      type: Number,
      require: true,
    },
    questionId: {
      type: Schema.Types.ObjectId,
      ref: Names.QUESTION,
      require: true,
    },
    answerId: {
      type: Schema.Types.ObjectId,
      ref: Names.ANSWER,
      require: false,
    },
    lastAnsweredIndex: {
      type: Number,
      require: true,
    },
    lastFetchTime: {
      type: Date,
      default: new Date(),
    },
    canPlay: {
      type: Number,
      required: true,
    },
    challengeStatus: {
      type: String,
      enum: ['Passed', 'Failed'],
      require: true,
    },
    correctors: {
      type: Number,
      required: true,
      default: 0,
    },
    correctorUsedTimes: {
      type: Number,
      required: true,
      default: 0,
    },
  },
  {
    timestamps: true,
  },
);
UserTrackSchema.plugin(mongooseDelete, { deletedAt: true });
export default UserTrackSchema;
