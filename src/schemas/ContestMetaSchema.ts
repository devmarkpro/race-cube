import { Schema } from 'mongoose';
import mongooseDelete from 'mongoose-delete';

const ContestSchema: Schema = new Schema(
  {
    appId: String,
    title: String,
    prize: Number,
    beginTime: Date,
    duration: Number,
    itemDuration: Number,
    neededCorrectors: Number,
    allowedCorrectorUsageTimes: Number,
    allowCorrectTilQuestion: Number,
    neededTickets: Number,
    questions: [
      {
        _id: {
          type: Schema.Types.ObjectId,
        },

        order: Number,
        text: String,
        options: [
          {
            _id: {
              type: Schema.Types.ObjectId,
            },
            order: Number,
            text: String,
            createdAt: {
              type: Date,
              default: new Date(),
            },
          },
        ],
        createdAt: {
          type: Date,
          default: new Date(),
        },
      },
    ],
    correctAnswers: [Schema.Types.ObjectId],
  },
  {
    timestamps: true,
  },
);
ContestSchema.plugin(mongooseDelete, { deletedAt: true });
export default ContestSchema;
