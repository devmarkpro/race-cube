export default {
  ANSWER: 'Answer',
  CONTEST: 'Contest',
  QUESTION: 'Question',
  USER_TRACK: 'UserTrack',
  USER_CREDIT: 'UserCredit',
};
