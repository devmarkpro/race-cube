import { Schema } from 'mongoose';
import mongooseDelete from 'mongoose-delete';

const UserCreditSchema: Schema = new Schema(
  {
    userId: Schema.Types.ObjectId,
    correctors: Number,
    tickets: Number,
  },
  {
    timestamps: true,
  },
);
UserCreditSchema.plugin(mongooseDelete, { deletedAt: true });
export default UserCreditSchema;
