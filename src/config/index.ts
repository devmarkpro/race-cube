import * as app from './App';
import * as log from './Log';
export const configs = {
  app,
  log,
};
