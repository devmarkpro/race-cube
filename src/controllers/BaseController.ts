import * as Models from '../schemas';
import * as ApplicationException from '../utils/Error';
import hasAccess from '../utils/AccessLevel';
import { configs } from '../config';
import * as logger from '../utils/Logger';
import Queues from '../jobs/Queue';
import GetJobHanderInstance from '../services/JobHandler';
export interface ResponseModel<T> {
  data: T;
  error: string | null;
}
export class BaseController {
  public Models = Models;
  public HasAccess = hasAccess;
  protected MakeError = ApplicationException.MakeError;
  protected Errors = ApplicationException.AppErrors;
  protected Configs = configs;
  protected Logger = logger;
  protected Queues = Queues;
  protected CreateJobHandler = GetJobHanderInstance;
  public Response<T>(model: T, omitProps: string[] = []): ResponseModel<T> {
    return {
      data: model,
      error: null,
    };
  }

  public ErrorResponse(error: string): ResponseModel<any> {
    return {
      data: null,
      error,
    };
  }
}
