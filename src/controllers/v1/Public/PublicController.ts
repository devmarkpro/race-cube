import { BaseController, ResponseModel } from '../../BaseController';
import { UserService } from '../../../services/User';
import { GenerateTimeline } from '../helpers/ContestHelper';
import moment from 'moment';
import { GetTodayContests } from '../../../services/ContestService';
import { ContestMetaModel } from '../../../models';

export class PublicContestController extends BaseController {
  private userService: UserService;
  constructor() {
    super();
    this.userService = new UserService();
  }
  public GetMeta = async (
    userId: string,
    appId?: string,
  ): Promise<ResponseModel<any>> => {
    if (!appId) {
      throw this.MakeError(this.Errors.APP_IS_REQUIRED);
    }
    const todayContests = await GetTodayContests(appId);
    if (!todayContests || todayContests.length === 0) {
      return this.Response([]);
    }
    this.userService.id = userId;
    const user = await this.userService.getUserData();
    const timeline = GenerateTimeline(
      (todayContests as unknown) as ContestMetaModel[],
    );
    const nextContest = timeline.filter(a => a.isCurrent)[0];
    const isInDeadline =
      moment(new Date(nextContest.startTime)).add(
        -1 * this.Configs.app.DeadlineSeconds,
        'seconds',
      ) <= moment();
    const res = {
      hoursToNextContest: moment(new Date(nextContest.startTime)).hours(),
      minToNextContest: moment(new Date(nextContest.startTime)).minutes(),
      nextRoundPrice: isInDeadline
        ? nextContest.prize
        : todayContests
            .map((a: any) => {
              const num = Number(a.prize);
              return !isNaN(num) ? num : 0;
            })
            .reduce((m, n) => m + n, 0),
      userCredit: user.tickets,
      correctorCount: user.correctors,
      isInDeadline,
      secondsToStart: moment(new Date(nextContest.startTime)).diff(
        moment(),
        'seconds',
      ),
      videoUrl: user.promoteVideoUrl || this.Configs.app.PromoteVideoUrl,
      shareData: {
        url: user.referral.url,
        title: user.referral.title,
        message: user.referral.message,
        dialogTitle: user.referral.dialog,
      },
      timeline: GenerateTimeline(
        (todayContests as unknown) as ContestMetaModel[],
      ),
    };

    return this.Response(res);
  };
}
