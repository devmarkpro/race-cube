import { PublicContestController } from './PublicController';
import { AdminContestController } from '../Admin/AdminContestController';
import moment from 'moment';
process.env.TEST_SUITE = 'contest-test';

describe('# => public methods', () => {
  describe('## => timeline', () => {
    test('### => should return timeline', async () => {
      const contestController = new PublicContestController();
      const adminContestController = new AdminContestController();
      const start1 = moment()
        .add(3, 'hours')
        .toDate();
      const start2 = moment()
        .add(1, 'hours')
        .toDate();
      const start3 = moment()
        .add(2, 'hours')
        .toDate();
      await adminContestController.AddMeta({
        appId: 'moneyket',
        title: 'contest1',
        prize: 500000,
        beginTime: start1,
        duration: 100,
        neededCorrectors: 1,
        allowedCorrectorUsageTimes: 1,
        allowCorrectTilQuestion: 10,
        neededTickets: 2,
      });

      await adminContestController.AddMeta({
        appId: 'moneyket',
        title: 'contest2',
        prize: 500000,
        beginTime: start2,
        duration: 100,
        neededCorrectors: 1,
        allowedCorrectorUsageTimes: 1,
        allowCorrectTilQuestion: 10,
        neededTickets: 2,
      });

      await adminContestController.AddMeta({
        appId: 'moneyket',
        title: 'contest3',
        prize: 500000,
        beginTime: start3,
        duration: 100,
        neededCorrectors: 1,
        allowedCorrectorUsageTimes: 1,
        allowCorrectTilQuestion: 10,
        neededTickets: 2,
      });
      const result = await contestController.GetMeta('', 'moneyket');
      const timeline = result.data.timeline;
      expect(result.error).toBeNull();
      expect(timeline.length).toBe(3);
      expect(timeline[0].text).toBe('contest2');
      expect(timeline[1].text).toBe('contest3');
      expect(timeline[2].text).toBe('contest1');
    });
  });
});
