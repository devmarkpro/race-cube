import { ContestMetaModel } from '../../../models/ContestMetaModel';
import { ContestTimelineResponse } from 'app-response';
import moment from 'moment-timezone';
import { findNearestDate } from '../../../utils/DateHelper';
import { sortBy } from 'lodash';
export const GenerateTimeline = (
  contests: ContestMetaModel[],
): ContestTimelineResponse[] => {
  const timeline: ContestTimelineResponse[] = [];
  contests.forEach((contest: ContestMetaModel, idx: number) => {
    timeline.push({
      text: contest.title,
      isPast: moment(contest.beginTime).isBefore(moment()),
      isCurrent:
        findNearestDate(
          contests.map(a => new Date(a.beginTime)),
          new Date(),
        ) === idx,
      startTime: moment(contest.beginTime)
        .tz('Asia/Tehran')
        .toString(),
      prize: contest.prize,
      currency: 'تومان',
    });
  });
  return sortBy(timeline, (r: ContestTimelineResponse) => r.startTime);
};
