import express from 'express';
import { AdminContestController } from './AdminContestController';

const router = express.Router();
const contestController = new AdminContestController();

router.post(
  '/meta',
  async (
    req: express.Request,
    res: express.Response,
    next: express.NextFunction,
  ) => {
    try {
      if (!contestController.HasAccess('/admin/contest', req.user.access)) {
        res.status(401);
      }
      const result = await contestController.AddMeta({
        appId: req.body.appId,
        title: req.body.title,
        prize: Number(req.body.prize),
        beginTime: new Date(req.body.beginTime),
        duration: Number(req.body.duration),
        neededCorrectors: Number(req.body.neededCorrectors),
        allowedCorrectorUsageTimes: Number(req.body.allowedCorrectorUsageTimes),
        allowCorrectTilQuestion: Number(req.body.allowCorrectTilQuestion),
        neededTickets: Number(req.body.neededTickets),
      });
      res.status(200).send(result);
    } catch (err) {
      next(err);
    }
  },
);
router.post(
  '/question',
  async (
    req: express.Request,
    res: express.Response,
    next: express.NextFunction,
  ) => {
    try {
      if (!contestController.HasAccess('/admin/contest', req.user.access)) {
        res.status(401);
      }
      const result = await contestController.AttachQuestions(req.body);
      res.status(200).send(result);
    } catch (err) {
      next(err);
    }
  },
);
router.delete(
  '/',
  async (
    req: express.Request,
    res: express.Response,
    next: express.NextFunction,
  ) => {
    try {
      if (!contestController.HasAccess('/admin/contest', req.user.access)) {
        res.status(401);
      }
      const result = await contestController.DeleteContest(req.body.id);
      res.status(200).send(result);
    } catch (err) {
      next(err);
    }
  },
);
router.put(
  '/',
  async (
    req: express.Request,
    res: express.Response,
    next: express.NextFunction,
  ) => {
    try {
      if (!contestController.HasAccess('/admin/contest', req.user.access)) {
        res.status(401);
      }
      const result = await contestController.UpdateContest(req.body);
      res.status(200).send(result);
    } catch (err) {
      next(err);
    }
  },
);
export default router;
