import express from 'express';
import ContestRoutes from './ContestRoutes';
const router = express.Router();

router.use('/contest', ContestRoutes);
export default router;
