import { BaseController, ResponseModel } from '../../BaseController';
import { GetTodayActiveContests } from '../../../services/ContestService';
import { AppConfig } from 'app-response';
import crypto from 'crypto';

export class UserController extends BaseController {
  public async Config(
    appId?: string,
    token?: string,
  ): Promise<ResponseModel<AppConfig>> {
    if (!appId) {
      throw this.MakeError(this.Errors.APP_IS_REQUIRED);
    }
    const todayContests = await GetTodayActiveContests(appId);
    const response: AppConfig = {};
    response.channels = [];
    if (todayContests.length > 0) {
      response.channels.push(todayContests[0]._id.toString());
      response.channels.push(`${todayContests[0]._id.toString()}:room`);
    }
    if (token) {
      const e = crypto
        .createHmac('sha512', this.Configs.app.TOPIC_ENCRYPTION_KEY)
        .update(token)
        .digest('hex');
      response.channels.push(`u:${e}`);
    }
    return this.Response(response);
  }
}
