import express from 'express';
import { UserController } from './UserController';
const router = express.Router();
const userController = new UserController();
router.get(
  '/config',
  async (
    req: express.Request,
    res: express.Response,
    next: express.NextFunction,
  ) => {
    try {
      const result = await userController.Config(
        req.header('Moneyket-APP'),
        req.header('Authorization'),
      );
      res.status(200).send(result);
    } catch (err) {
      next(err);
    }
  },
);
export default router;
