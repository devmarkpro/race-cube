export interface AddMetaResponse {
  id: string;
}
export interface AttachQuestionResponse {
  _id: string;
  title: string;
  prize: number;
  duration: number;
  neededCorrectors: number;
  allowedCorrectorUsageTimes: number;
  allowCorrectTilQuestion: number;
  neededTickets: number;
  questions: QuestionResponse[];
}
export interface QuestionResponse {
  _id: string;
  text: string;
  order: number;
  options: AnswerResponse[];
}
export interface AnswerResponse {
  _id: string;
  text: string;
  order: number;
}
export interface QuestionMetaResponse {
  hoursToNextContest: number;
  minToNextContest: number;
  nextRoundPrice: number;
  userCredit: number;
  incomeTicketCount: number;
  correctorCount: number;
  isInDeadline: boolean; // set true in a short time before start contest
  secondsToStart: number; // Use when isInDeadline is true; Show a countdown to start the game
  videoUrl: string;
  shareData: {
    // Use for invite friends
    url: string;
    title: string;
    message: string;
    dialogTitle: string;
  };
}
export interface ContestTimelineResponse {
  text: string;
  isPast: boolean;
  isCurrent: boolean;
  startTime: string;
  prize: number;
  currency: string;
}
export interface ContestMetaResponse {
  hoursToNextContest: number;
  minToNextContest: number;
  nextRoundPrice: number;
  userCredit: number;
  incomeTicketCount: number;
  correctorCount: number;
  isInDeadline: boolean; // set true in a short time before start contest
  secondsToStart: number; // Use when isInDeadline is true; Show a countdown to start the game
  videoUrl: string;
  shareData: {
    // Use for invite friends
    url: string;
    title: string;
    message: string;
    dialogTitle: string;
  };
  timeline: ContestTimelineResponse[];
}

export interface AppConfig {
  channels?: string[];
}
export interface MessageResponse {
  type: 'QUESTION' | 'DEAD_LINE' | 'CHAT' | 'LEADER_BOARD';
  data: object;
}
