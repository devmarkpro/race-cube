FROM node:10.15.0-alpine

ARG BUILD_DATE
ARG COMMIT_REF_SLUG
ARG COMMIT_SHORT_SHA

ENV PORT 3000

LABEL maintainer="m.abdolirad@gmail.com" \
    org.label-schema.schema-version="1.0" \
    org.label-schema.build-date=$BUILD_DATE \
    org.label-schema.name="Shop" \
    org.label-schema.description="Shop service API." \
    org.label-schema.url="https://gitlab.com/mt-api/shop" \
    org.label-schema.vcs-url="https://gitlab.com/mt-api/shop" \
    org.label-schema.vcs-ref=$COMMIT_SHORT_SHA \
    org.label-schema.vendor="MoneyKet" \
    org.label-schema.version=$COMMIT_REF_SLUG

WORKDIR /app

COPY ./dist/bundle.js ./
COPY ./node_modules ./node_modules/

EXPOSE 3000

CMD ["node", "/app/bundle.js"]
